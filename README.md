# Promesas a observables

A continuación, se detallará una manera sencilla de transformar promesas a observables, para esto, se hace referencia al proyecto [Callbacks, promesas y observables](https://gitlab.com/manticore-labs/open-source/investigacion/callbacks-promesas-observables) para hacer uso de los ejemplos allí descritos. 

## Pasos a seguir

* Al igual que en el proyecto antes mencionado, el primer paso consiste en instalar la librería *_rxjs_* a través del comando:
  ```
  npm i rxjs
  ```
* Con la librería instalada, es necesario llamar las variables de observables a utilizar. Además de esto, será necesario llamar a la función *_fromPromise_* que se encuentra en la siguiente dirección de la librería:
  ```
  'rxjs/add/observable/fromPromise'
  ```
* Finalmente, es necesario crear dos funciones: la primera relacionada a la promesa y la segunda al oservable.

## Ejemplo

A continuación se muestra un pequeño script en JavaScript que muestra como quedan estructurados los elementos. Cabe mencionar que para acceder al objeto, se lo hará como observable a pesar de originalmente se trate de una promesa. Para aclarar esto, se tiene:

```
const estudiante = {
    "nombre": "Andrés Torres Albuja",
    "todosCreditosAprobados": false
}

function darExamenFinCarreraPromesa(estudiante) {
    return new Promise((response, reject) => {
         if(estudiante.todosCreditosAprobados) {
            resolve({
                Estudiante: estudiante.nombre,
                TodosCreditosAprobados: estudiante.todosCreditosAprobados, 
                Mensaje: "Preséntese a dar el exámen de fin de carrera"
            })
        }

        else {
            reject({
                Estudiante: estudiante.nombre,
                TodosCreditosAprobados: estudiante.TodosCreditosAprobados, 
                Mensaje: "No puede dar el examen de fin de carrera"
            })
        }

    })
}

function darExamenFinCarreraObservable() {
    return Observable.fromPromise(this.darExamenFinCarreraPromesa(estudiante)).pipe(
                    map({ nombre } => nombre)
                )

}

const promesaAObservable$ = darExamenFinCarreraObservable().subscribe((respuesta) => {
    console.log(respuesta);
}) 
```

Como se puede observar, gracias al uso de la librería rxjs, el paso de promesa a observable es bastante sencillo, el único inconveniente en JavaScript se debe a los *_require_* debido a la versión de dicha librería, y no se conseguió que el programa se ejecutara correctamente con las versiones más actuales.

<a href="https://twitter.com/following" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @BAndresTorres </a><br>
<a href="https://www.facebook.com/bryan.a.torres.5" target="_blank"><img alt="Sígueme en Facebook" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en Facebook"/> Andrés Torres Albuja </a><br>
<a href="https://www.instagram.com/adler.luft/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> adler.luft </a><br>
